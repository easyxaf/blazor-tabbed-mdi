﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Blazor;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;

namespace EasyXaf.BlazorTabbedMdi;

public abstract class MdiBlazorApplication : BlazorApplication
{
    protected override IFrameTemplate CreateDefaultTemplate(TemplateContext context)
    {
        if (context == TemplateContext.ApplicationWindow)
        {
            return new MdiMainWindowTemplate(MainWindow) { AboutInfoString = AboutInfo.Instance.GetAboutInfoString(this) };
        }
        return base.CreateDefaultTemplate(context);
    }

    protected override ShowViewStrategyBase CreateShowViewStrategy()
    {
        return new MdiBlazorShowViewStrategy(this);
    }
}
