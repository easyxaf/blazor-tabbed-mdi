﻿using DevExpress.ExpressApp;
using System.Collections.ObjectModel;

namespace EasyXaf.BlazorTabbedMdi;

public class WindowGroup
{
    public const string DefaultGroupName = "DefaultGroup";

    public string Name { get; set; }

    public bool IsDefaultGroup => Name == DefaultGroupName;

    public Window CurrentWindow { get; private set; }

    public int CurrentWindowIndex
    {
        get => Windows.IndexOf(CurrentWindow);
        set
        {
            if (Windows.Any())
            {
                var index = Math.Max(0, value);
                index = Math.Min(index, Windows.Count - 1);
                CurrentWindow = Windows[index];
            }
            else
            {
                CurrentWindow = null;
            }
        }
    }

    public bool HasUnsavedChanges => Windows.Any(w => w.Template is IMdiFrameTemplate t && t.HasUnsavedChanges);

    public ObservableCollection<Window> Windows { get; }

    public WindowGroup()
    {
        Windows = new ObservableCollection<Window>();
    }

    public void SetCurrentWindow(Window window)
    {
        if (Windows.Contains(window))
        {
            CurrentWindow = window;
        }
    }
}
