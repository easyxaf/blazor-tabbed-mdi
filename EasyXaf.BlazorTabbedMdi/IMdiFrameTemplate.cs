﻿using DevExpress.ExpressApp.Templates;

namespace EasyXaf.BlazorTabbedMdi;

public interface IMdiFrameTemplate : IFrameTemplate
{
    bool HasUnsavedChanges { get; }

    event EventHandler StateChanged;
}
