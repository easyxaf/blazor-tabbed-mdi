﻿using DevExpress.ExpressApp;

namespace EasyXaf.BlazorTabbedMdi;

public static class WindowExtensions
{
    public static string GetCaption(this Window window)
    {
        string caption;
        if (window.View is DetailView view)
        {
            caption = view.GetCurrentObjectCaption();
            caption = string.IsNullOrWhiteSpace(caption) ? view.Caption : caption;
        }
        else
        {
            caption = window.View.Caption;
        }
        if (string.IsNullOrWhiteSpace(caption))
        {
            caption = window.View.ObjectTypeInfo.Name;
        }
        return caption;
    }
}
