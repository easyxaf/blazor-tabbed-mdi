﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace EasyXaf.BlazorTabbedMdi;

public class CloseListViewController : ViewController<ListView>
{
    public SimpleAction CloseAction { get; }

    public CloseListViewController()
    {
        TargetViewNesting = Nesting.Root;

        CloseAction = new SimpleAction(this, "CloseListView", "Close")
        {
            Caption = "Close",
            ImageName = "Action_Close"
        };

        CloseAction.Execute += CloseAction_Execute;
    }

    private void CloseAction_Execute(object sender, SimpleActionExecuteEventArgs e)
    {
        Close(e);
    }

    protected virtual void Close(SimpleActionExecuteEventArgs args)
    {
        View.Close();
    }
}
