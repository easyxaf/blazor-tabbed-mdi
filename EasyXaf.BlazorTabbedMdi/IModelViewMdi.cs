﻿using DevExpress.ExpressApp.Model.Core;
using DevExpress.Persistent.Base;
using System.ComponentModel;

namespace EasyXaf.BlazorTabbedMdi;

public interface IModelViewMdi
{
    [Category("Mdi")]
    public string MdiGroupName { get; set; }

    [Browsable(false)]
    public IEnumerable<Type> MdiWindowTemplateTypes { get; }

    [Category("Mdi")]
    [DataSourceProperty(nameof(MdiWindowTemplateTypes))]
    [TypeConverter(typeof(StringToTypeConverterBase))]
    public Type MdiWindowTemplateType { get; set; }
}
