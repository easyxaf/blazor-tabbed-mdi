﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Blazor.SystemModule;

namespace EasyXaf.BlazorTabbedMdi;

public static class FrameExtensions
{
    public static void SetControllerActive<TController>(this Frame frame, bool active)
        where TController : Controller
    {
        frame.GetController<TController>()?.Active.SetItemValue(frame.GetType().ToString(), active);
    }

    public static void SetCloseActionActive(this Frame frame, bool active)
    {
        if (frame.View is ListView)
        {
            frame.SetControllerActive<CloseListViewController>(active);
        }
        else if (frame.View is DetailView)
        {
            frame.SetControllerActive<CloseDetailViewController>(active);
        }
        else
        {
            frame.SetControllerActive<CloseDashboardViewViewController>(active);
        }
    }

    public static string GetImageName(this Frame frame)
    {
        var imageName = string.Empty;

        if (frame.View is ObjectView objectView)
        {
            imageName = objectView.Model.ModelClass.ImageName;
            //if (string.IsNullOrWhiteSpace(imageName) && objectView.CurrentObject is IHasImageName hasImageName)
            //{
            //    imageName = hasImageName.ImageName;
            //}
        }

        if (string.IsNullOrWhiteSpace(imageName))
        {
            imageName = "Actions_Window";
        }

        return imageName;
    }
}
