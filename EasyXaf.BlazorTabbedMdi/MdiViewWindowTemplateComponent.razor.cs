﻿using DevExpress.ExpressApp.Blazor.Templates;

namespace EasyXaf.BlazorTabbedMdi;

public partial class MdiViewWindowTemplateComponent : FrameTemplateComponentBase<MdiViewWindowTemplate>
{
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            FrameTemplate.StateChanged += (s, e) => StateHasChanged();
        }

        FrameTemplate.DisposeableViews.ForEach(v => v.Dispose());
        FrameTemplate.DisposeableViews.Clear();

        FrameTemplate.DisposeableWindows.ForEach(v => v.Dispose());
        FrameTemplate.DisposeableWindows.Clear();
    }
}
