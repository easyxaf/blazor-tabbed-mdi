﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;

namespace EasyXaf.BlazorTabbedMdi;

public sealed class TabbedMdiModule : ModuleBase
{
    public override void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
    {
        base.ExtendModelInterfaces(extenders);

        extenders.Add<IModelApplication, IModelApplicationMdi>();
        extenders.Add<IModelView, IModelViewMdi>();
    }

    protected override IEnumerable<Type> GetDeclaredControllerTypes()
    {
        return new Type[]
        {
            typeof(CloseDashboardViewViewController),
            typeof(CloseListViewController),
            typeof(MdiWindowListViewController),
            typeof(MdiWindowViewController)
        };
    }

    protected override IEnumerable<Type> GetDeclaredExportedTypes()
    {
        return new Type[]
        {
            typeof(MdiWindow),
            typeof(MdiWindowList)
        };
    }
}
