﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;

namespace EasyXaf.BlazorTabbedMdi;

public class MdiWindowListViewController : ObjectViewController<DetailView, MdiWindowList>
{
    protected override void OnActivated()
    {
        base.OnActivated();

        var dialogController = Frame.GetController<DialogController>();
        if (dialogController != null)
        {
            dialogController.CancelAction.Active["Visible"] = false;
        }
    }
}
