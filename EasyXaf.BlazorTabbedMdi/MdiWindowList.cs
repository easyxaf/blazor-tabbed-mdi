﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using System.ComponentModel;

namespace EasyXaf.BlazorTabbedMdi;

[DomainComponent]
[DisplayName("窗口")]
public class MdiWindowList
{
    [ModelDefault("Caption", "")]
    public List<MdiWindow> Windows { get; }

    public MdiWindowList()
    {
        Windows = new List<MdiWindow>();
    }
}
