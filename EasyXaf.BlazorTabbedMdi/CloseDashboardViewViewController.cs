﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace EasyXaf.BlazorTabbedMdi;

public class CloseDashboardViewViewController : ViewController<DashboardView>
{
    public SimpleAction CloseAction { get; }

    public CloseDashboardViewViewController()
    {
        TargetViewNesting = Nesting.Root;

        CloseAction = new SimpleAction(this, "CloseDashboardView", "Close")
        {
            Caption = "Close",
            ImageName = "Action_Close"
        };

        CloseAction.Execute += CloseAction_Execute;
    }

    private void CloseAction_Execute(object sender, SimpleActionExecuteEventArgs e)
    {
        Close(e);
    }

    protected virtual void Close(SimpleActionExecuteEventArgs args)
    {
        View.Close();
    }
}
