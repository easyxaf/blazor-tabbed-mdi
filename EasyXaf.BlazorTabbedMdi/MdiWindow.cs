﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Data;
using DevExpress.ExpressApp.DC;
using System.ComponentModel;

namespace EasyXaf.BlazorTabbedMdi;

[DomainComponent]
public class MdiWindow : INotifyPropertyChanged
{
    [Key]
    [Browsable(false)]
    public Guid Id { get; }

    [DisplayName("标题")]
    public string Caption => $"{Window.GetCaption()}{(HasUnsavedChanges ? "*" : string.Empty)}";

    [DisplayName("未保存")]
    public bool HasUnsavedChanges
    {
        get
        {
            if (Window.Template is MdiViewWindowTemplate template)
            {
                return template.HasUnsavedChanges;
            }
            return false;
        }
    }

    [Browsable(false)]
    public Window Window { get; }

    public event PropertyChangedEventHandler PropertyChanged;

    public MdiWindow(Window window)
    {
        Id = Guid.NewGuid();
        Window = window;
    }

    public void RaisePropertyChanged()
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Caption)));
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasUnsavedChanges)));
    }
}
