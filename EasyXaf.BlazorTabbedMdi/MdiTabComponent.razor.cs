using DevExpress.Blazor;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Blazor.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace EasyXaf.BlazorTabbedMdi;

public partial class MdiTabComponent
{
    private WindowGroup CurrentWindowGroup => WindowTemplate.CurrentWindowGroup;

    private int CurrentWindowIndex => CurrentWindowGroup.CurrentWindowIndex;

    private string TabPageCloseClass => WindowTemplate.AllWindows.Count() > 1 ? "close" : "close hide";

    private DxContextMenu MdiContextMenu { get; set; }

    private Window MdiContextMenuWindow { get; set; }

    private bool IsMoveToNewWindowEnabled { get; set; }

    private bool IsCloseTabEnabled { get; set; }

    private bool IsCloseLeftTabsEnabled { get; set; }

    private bool IsCloseRightTabsEnabled { get; set; }

    private bool IsCloseOtherTabsEnabled { get; set; }

    private bool IsCloseAllTabsEnabled { get; set; }

    [Inject]
    private IJSRuntime JSRuntime { get; set; }

    [Inject]
    private IImageUrlService ImageUrlService { get; set; }

    [Parameter]
    public MdiMainWindowTemplate WindowTemplate { get; set; }

    private async Task MoveTabToNewWindow()
    {
        if (MdiContextMenuWindow != null)
        {
            var url = MdiContextMenuWindow.View.Id;
            if (MdiContextMenuWindow.View is DetailView detailView)
            {
                url = $"{url}/{detailView.ObjectSpace.GetKeyValueAsString(detailView.CurrentObject)}";
            }

            if (await WindowTemplate.CloseWindowAsync(MdiContextMenuWindow))
            {
                await JSRuntime.InvokeVoidAsync("open", CancellationToken.None, url, "_blank");
            }
        }
    }

    private async Task CloseTabAsync()
    {
        if (MdiContextMenuWindow != null)
        {
            await WindowTemplate.CloseWindowAsync(MdiContextMenuWindow);
        }
    }

    private async Task CloseLeftTabsAsync()
    {
        if (MdiContextMenuWindow != null)
        {
            await WindowTemplate.CloseLeftWindowsAsync(MdiContextMenuWindow);
        }
    }

    private async Task CloseRightTabsAsync()
    {
        if (MdiContextMenuWindow != null)
        {
            await WindowTemplate.CloseRightWindowsAsync(MdiContextMenuWindow);
        }
    }

    private async Task CloseOtherTabsAsync()
    {
        if (MdiContextMenuWindow != null)
        {
            await WindowTemplate.CloseOtherWindowsAsync(MdiContextMenuWindow);
        }
    }

    private async Task CloseAllTabsAsync()
    {
        await WindowTemplate.CloseAllWindowsAsync();
    }

    private async Task ShowMdiContextMenuAsync(Window window, MouseEventArgs args)
    {
        MdiContextMenuWindow = window;

        IsMoveToNewWindowEnabled = false;
        IsCloseTabEnabled = false;
        IsCloseLeftTabsEnabled = false;
        IsCloseRightTabsEnabled = false;
        IsCloseOtherTabsEnabled = false;
        IsCloseAllTabsEnabled = false;

        var windows = WindowTemplate.CurrentGroupWindows;
        if (WindowTemplate.AllWindows.Count() > 1 && windows.Any())
        {
            IsMoveToNewWindowEnabled = true;
            IsCloseTabEnabled = true;
            IsCloseAllTabsEnabled = true;

            if (windows.IndexOf(window) > 0)
            {
                IsCloseLeftTabsEnabled = true;
            }

            if (windows.IndexOf(window) < windows.Count - 1)
            {
                IsCloseRightTabsEnabled = true;
            }

            if (windows.Count > 1)
            {
                IsCloseOtherTabsEnabled = true;
            }

            if (window.View is DetailView && window.View.ObjectSpace.IsNewObject(window.View.CurrentObject))
            {
                IsMoveToNewWindowEnabled = false;
            }
        }

        await MdiContextMenu.ShowAsync(args);
    }
}
