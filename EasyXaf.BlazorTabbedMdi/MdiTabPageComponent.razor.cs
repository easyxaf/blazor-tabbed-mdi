using DevExpress.ExpressApp;
using Microsoft.AspNetCore.Components;

namespace EasyXaf.BlazorTabbedMdi;

public partial class MdiTabPageComponent
{
    [Parameter]
    public Window Window { get; set; }

    protected override bool ShouldRender()
    {
        return false;
    }
}