﻿using System.ComponentModel;

namespace EasyXaf.BlazorTabbedMdi;

public interface IModelApplicationMdi
{
    [Category("Mdi")]
    [DefaultValue(true)]
    public bool IsWindowKeepOpen { get; set; }

    [Category("Mdi")]
    [DefaultValue(true)]
    public bool IsShowTabbedMdi { get; set; }

    [Category("Mdi")]
    [DefaultValue(false)]
    public bool EnableMdiGroup { get; set; }
}
