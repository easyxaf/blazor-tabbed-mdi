﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using EasyXaf.Sample.Module.BusinessObjects;

namespace EasyXaf.Sample.Module.Controllers;

public class ApplicationUserViewController : ObjectViewController<ObjectView, ApplicationUser>
{
    public IEnumerable<SimpleAction> UserTestActions { get; }

    public ApplicationUserViewController()
    {
        var actions = new List<SimpleAction>();
        for (var i = 0; i < 5; i++)
        {
            actions.Add(new SimpleAction(this, $"UserTestAction{i + 1}", "Toolbar2")
            {
                Caption = $"测试工具{i + 1:00}",
                ImageName = "Action_New",
            });
        }
        UserTestActions = actions.AsReadOnly();
    }
}
