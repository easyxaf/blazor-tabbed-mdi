﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;

namespace EasyXaf.Sample.Blazor.Server.Controllers;

public class MainWindowController : WindowController
{
    public SimpleAction NotificationAction { get; }

    public MainWindowController()
    {
        TargetWindowType = WindowType.Main;
        NotificationAction = new SimpleAction(this, "NotificationAction", PredefinedCategory.Notifications)
        {
            ImageName = "BO_Notifications",
            Caption = "通知"
        };
    }

    protected override void OnActivated()
    {
        base.OnActivated();

        NotificationAction.Execute += NotificationAction_Execute;
    }

    protected override void OnDeactivated()
    {
        NotificationAction.Execute -= NotificationAction_Execute;

        base.OnDeactivated();
    }

    private void NotificationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
    {
        Application.ShowViewStrategy.ShowMessage("Blazor Tabbed MDI");
    }
}
